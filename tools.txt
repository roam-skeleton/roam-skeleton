$CC $PROG $PROG_MIN
/usr/bin/perl perl5/full/skeleton.pl perl5/min/skeleton-min.pl perl5/func/skeleton-func.pl
python2.6 python/v2.7/full/skeleton.sh python/v2.7/min/skeleton-min.sh python/v2.7/mod/scripts/skeleton-min.sh
python2.7 python/v2.7/full/skeleton.sh python/v2.7/min/skeleton-min.sh python/v2.7/mod/scripts/skeleton-min.sh
python3.4 python/v2.7/full/skeleton.sh python/v2.7/min/skeleton-min.sh python/v2.7/mod/scripts/skeleton-min.sh
python3.5 python/v2.7/full/skeleton.sh python/v2.7/min/skeleton-min.sh python/v2.7/mod/scripts/skeleton-min.sh
python3.6 python/v2.7/full/skeleton.sh python/v2.7/min/skeleton-min.sh python/v2.7/mod/scripts/skeleton-min.sh
python3.7 python/v2.7/full/skeleton.sh python/v2.7/min/skeleton-min.sh python/v2.7/mod/scripts/skeleton-min.sh
python3.8 python/v2.7/full/skeleton.sh python/v2.7/min/skeleton-min.sh python/v2.7/mod/scripts/skeleton-min.sh
python3.4 python/v3.4/full/skeleton.sh python/v3.4/min/skeleton-min.sh python/v3.4/mod/scripts/skeleton-min.sh
python3.5 python/v3.4/full/skeleton.sh python/v3.4/min/skeleton-min.sh python/v3.4/mod/scripts/skeleton-min.sh
python3.6 python/v3.4/full/skeleton.sh python/v3.4/min/skeleton-min.sh python/v3.4/mod/scripts/skeleton-min.sh
python3.7 python/v3.4/full/skeleton.sh python/v3.4/min/skeleton-min.sh python/v3.4/mod/scripts/skeleton-min.sh
python3.8 python/v3.4/full/skeleton.sh python/v3.4/min/skeleton-min.sh python/v3.4/mod/scripts/skeleton-min.sh
python3.5 python/latest/full/skeleton.sh python/latest/min/skeleton-min.sh python/latest/mod/scripts/skeleton-min.sh
python3.6 python/latest/full/skeleton.sh python/latest/min/skeleton-min.sh python/latest/mod/scripts/skeleton-min.sh
python3.7 python/latest/full/skeleton.sh python/latest/min/skeleton-min.sh python/latest/mod/scripts/skeleton-min.sh
python3.8 python/latest/full/skeleton.sh python/latest/min/skeleton-min.sh python/latest/mod/scripts/skeleton-min.sh
/bin/sh sh/full/skeleton.sh sh/min/skeleton-min.sh
cargo $PROG_RUST_FULL $PROG_RUST_FUNC
raku raku/min/skeleton-min.raku
