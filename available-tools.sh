#!/bin/sh
#
# Copyright (c) 2018, 2019  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

set -e

version_string='0.3.0'

version()
{
	echo "available-tools $version_string"
}

features()
{
	echo "Features: available-tools=$version_string"
}

usage()
{
	cat <<EOUSAGE
Usage:	available-tools [-N] check
	available-tools [-N] build
	available-tools [-N] test test-tool [test-tool-arg...]
	available-tools -V | -h | --version | --help 
	available-tools --features

	-h	display program usage information and exit
	-N	no operation mode; display what would have been done
	-V	display program version information and exit
EOUSAGE
}

unset hflag noop Vflag show_features

while getopts 'hNVv-:' o; do
	case "$o" in
		h)
			hflag=1
			;;

		N)
			noop='echo'
			;;

		V)
			Vflag=1
			;;

		-)
			if [ "$OPTARG" = 'help' ]; then
				hflag=1
			elif [ "$OPTARG" = 'version' ]; then
				Vflag=1
			elif [ "$OPTARG" = 'features' ]; then
				show_features=1
			else
				echo "Invalid long option '$OPTARG' specified" 1>&2
				usage 1>&2
				exit 1
			fi
			;;

		*)
			usage 1>&2
			exit 1
			;;
	esac
done
[ -z "$Vflag" ] || version
[ -z "$hflag" ] || usage
[ -z "$show_features" ] || features
[ -z "$Vflag$hflag$show_features" ] || exit 0

shift `expr "$OPTIND" - 1`
if [ "$#" -lt 1 ]; then
	usage 1>&2
	exit 1
fi
action="$1"
shift
case "$action" in
	check|build)
		if [ "$#" != 0 ]; then
			usage 1>&2
			exit 1
		fi
		;;

	test)
		if [ "$#" = 0 ]; then
			usage 1>&2
			exit 1
		fi
		;;

	*)
		usage 1>&2
		exit 1
		;;
esac

while read tool programs; do
	toolname="${tool#\$}"
	if [ "$toolname" != "$tool" ]; then
		tool="$(printenv -- "$toolname" | awk '{ print $1 }')"
		if [ -z "$tool" ]; then
			echo "No $toolname in the environment" 1>&2
			exit 1
		fi
	fi
	if ! command -v "$tool" > /dev/null 2>&1; then
		continue
	fi

	for prog in $programs; do
		varname="${prog#\$}"
		if [ "$varname" != "$prog" ]; then
			prog="$(printenv -- "$varname" || true)"
			if [ -z "$prog" ]; then
				echo "No $varname in the environment" 1>&2
				exit 1
			fi
		fi
		case "$action" in
			check)
				printf -- '%s\n' "$prog"
				;;

			build)
                printf -- '=== Building %s using %s\n' "$prog" "$tool"
				env TOOL_NAME="$tool" make -- "$prog"
				;;

			test)
                printf -- '=== Testing %s using %s\n' "$prog" "$tool"
				env TOOL_NAME="$tool" TEST_PROG="./$prog" "$@"
				;;

			*)
				echo "How did we get here?! ($action)" 1>&2
				exit 1
				;;
		esac
	done
done < tools.txt
