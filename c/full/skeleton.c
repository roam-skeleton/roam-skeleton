/*-
 * Copyright (c) 2013, 2015 - 2018  Peter Pentchev
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/types.h>
#include <sys/wait.h>

#include <ctype.h>
#include <err.h>
#include <errno.h>
#include <pwd.h>
#include <regex.h>
#include <signal.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#ifndef __printflike
#if defined(__GNUC__) && __GNUC__ >= 3
#define __printflike(x, y)	__attribute__((format(printf, (x), (y))))
#else
#define __printflike(x, y)
#endif
#endif

#ifndef __unused
#if defined(__GNUC__) && __GNUC__ >= 3
#define __unused	__attribute__((unused))
#else
#define __unused
#endif
#endif

#define VERSION_STRING	"0.1.0.dev1"

/* There's a reason for those values... */
enum pipe_dir {
	PIPE_DIR_WRITE = 0,
	PIPE_DIR_READ = 1,
};

static bool		verbose;
static char 		buf[4096];

static void
usage(const bool _ferr)
{
	const char * const s =
	    "Usage:\tskeleton [-Nv] [-n lines] [-S sig] [-s subject] [-u username] filename...\n"
	    "\tskeleton -V | -h | --version | --help\n"
	    "\tskeleton --features\n"
	    "\n"
	    "\t-h\tdisplay program usage information and exit\n"
	    "\t-N\tno operation mode; display what would have been done\n"
	    "\t-n\tspecify the number of lines to checksum for each file\n"
	    "\t-S\tspecify the signal (or NONE) to raise before actual work\n"
	    "\t-u\tmail the output to the specified user\n"
	    "\t-V\tdisplay program version information and exit\n"
	    "\t-v\tverbose operation; display diagnostic output\n";

	fprintf(_ferr? stderr: stdout, "%s", s);
	if (_ferr)
		exit(1);
}

static void
version(void)
{
	puts("skeleton " VERSION_STRING);
}

static void
features(void)
{
	puts("Features: skeleton=" VERSION_STRING);
}

static void
debug(const char * const fmt, ...)
{
	va_list v;

	va_start(v, fmt);
	if (verbose)
		vfprintf(stderr, fmt, v);
	va_end(v);
}

static void
pipe_prog(const enum pipe_dir dir, int * const pipefd, pid_t * const pid, char * const cmd[])
{
	int pfd[2];
	if (pipe(pfd) == -1)
		err(1, "Creating a pipe for %s", cmd[0]);
	const pid_t pp = fork();
	if (pp == -1) {
		err(1, "Forking for %s", cmd[0]);
	} else if (pp == 0) {
		if (close(pfd[1 - dir]) == -1)
			err(1, "Child: closing the 'other' fd %d",
			    pfd[1 - dir]);
		if (dup2(pfd[dir], dir) == -1)
			err(1, "Reopening standard %s as fd %d",
			    dir == PIPE_DIR_READ? "output": "input", pfd[dir]);
		execvp(cmd[0], cmd);
		err(1, "Executing %s", cmd[0]);
	}

	if (close(pfd[dir]) == -1)
		err(1, "Parent: closing the 'other' fd %d", pfd[dir]);
	memcpy(pipefd, pfd, sizeof(pfd));
	*pid = pp;
}

static void
check_wait_result(const pid_t pid, const int stat, const pid_t expected, const char * const progname)
{
	if (pid != expected)
		errx(1, "Waiting for %s: expected pid %ld, got %ld", progname, (long)expected, (long)pid);
	else if (WIFEXITED(stat) && WEXITSTATUS(stat) != 0)
		errx(1, "Child %s (pid %ld) exited with code %d", progname, (long)pid, WEXITSTATUS(stat));
	else if (WIFSIGNALED(stat))
		errx(1, "Child %s (pid %ld) was killed by signal %d", progname, (long)pid, WTERMSIG(stat));
	else if (WIFSTOPPED(stat))
		errx(1, "Child %s (pid %ld) was stopped by signal %d", progname, (long)pid, WSTOPSIG(stat));
	else if (!WIFEXITED(stat))
		errx(1, "Child %s (pid %ld) neither exited nor was killed or stopped; what in the world does wait(2) status %d mean?!", progname, (long)pid, stat);
}

static void
report_regerror(const int code)
{
	char rbuf[256];

	regerror(code, NULL, rbuf, sizeof(rbuf));
	errx(1, "Internal error compiling a regular expression: %s", rbuf);
}

static void
sighandler(int sig __unused) {
	_exit(2);
}

struct signal_and_name {
	int sig;
	const char *name;
};

struct signal_and_name signames[] = {
	{.sig = SIGINT , .name = "INT" ,},
	{.sig = SIGHUP , .name = "HUP" ,},
	{.sig = SIGQUIT, .name = "QUIT",},
	{.sig = SIGTERM, .name = "TERM",},
};
#define NSIGNAME (sizeof(signames) / sizeof(signames[0]))

static int
sigbyname(const char * const name) {
	for (size_t idx = 0; idx < NSIGNAME; idx++)
		if (strcmp(name, signames[idx].name) == 0)
			return (signames[idx].sig);
	return (-1);
}

static void
setup_signals(void)
{
	struct sigaction sa = {
		.sa_handler = sighandler,
		.sa_flags = 0,
	};
	sigemptyset(&sa.sa_mask);

	for (size_t idx = 0; idx < NSIGNAME; idx++)
		if (sigaction(signames[idx].sig, &sa, NULL) == -1)
			err(1, "Could not set the signal handler for SIG%s",
			    signames[idx].name);
}

int
main(int argc, char * const argv[])
{
	bool hflag = false, Vflag = false, show_features = false, noop = false;
	const char *lines = NULL, *subject = NULL, *uname = NULL;
	int sig = -1;
	int ch;
	while (ch = getopt(argc, argv, "hNn:S:s:u:Vv-:"), ch != -1)
		switch (ch) {
			case 'h':
				hflag = true;
				break;

			case 'N':
				noop = true;
				break;

			case 'n':
				lines = optarg;
				break;

			case 'V':
				Vflag = true;
				break;

			case 'S':
				if (strcmp(optarg, "NONE") == 0)
					sig = 0;
				else {
					sig = sigbyname(optarg);
					if (sig == -1)
						errx(1, "Unsupported signal name");
				}
				break;

			case 's':
				subject = optarg;
				break;

			case 'u':
				if (uname != NULL)
					errx(1, "Duplicate username/user ID specified");
				uname = optarg;
				break;

			case 'v':
				verbose = true;
				break;

			case '-':
				if (strcmp(optarg, "help") == 0)
					hflag = true;
				else if (strcmp(optarg, "version") == 0)
					Vflag = true;
				else if (strcmp(optarg, "features") == 0)
					show_features = true;
				else {
					warnx("Invalid long option '%s' specified", optarg);
					usage(true);
				}
				break;

			default:
				usage(1);
				/* NOTREACHED */
		}
	if (Vflag)
		version();
	if (hflag)
		usage(false);
	if (show_features)
		features();
	if (Vflag || hflag || show_features)
		return (0);

	argc -= optind;
	argv += optind;
	if (argc < 1)
		usage(true);

	setup_signals();

	if (sig > 0)
		raise(sig);

	/* Number of lines */
	if (lines != NULL) {

		if (!isdigit(*lines))
			errx(1, "Invalid number of lines specified");
		errno = 0;
		char *end;
		const long val = strtol(lines, &end, 10);
		if (errno != 0 || val < 1)
			errx(1, "Invalid number of lines specified");
	} else {
		lines = "10";
	}

	/* Handle username/user ID */
	if (uname != NULL) {
		regex_t ruid, runame;
		int rr;

		if (rr = regcomp(&ruid, "^(0|[1-9][0-9]*)$", REG_EXTENDED | REG_NOSUB), rr != 0)
			report_regerror(rr);
		if (rr = regcomp(&runame, "^[a-zA-Z0-9_.][a-zA-Z0-9_.]*$", REG_EXTENDED | REG_NOSUB), rr != 0)
			report_regerror(rr);

		if (regexec(&ruid, uname, 0, NULL, 0) == 0) {
			/* We have a number! */
			errno = 0;
			char *end;
			const unsigned long val = strtoul(uname, &end, 10);
			if (errno != 0 || *end != '\0')
				errx(1, "Invalid user ID specified");
			if (((unsigned long)((uid_t)-1)) < (unsigned long)-1)
				if (val > (uid_t)-1)
					errx(1, "User ID too large");

			const struct passwd * const pw = getpwuid((uid_t)val);
			if (pw == NULL)
				errx(1, "Nonexistent user ID specified");
			uname = pw->pw_name;
		} else if (regexec(&runame, uname, 0, NULL, 0) == REG_NOMATCH) {
			errx(1, "Invalid user name or ID specified");
		}

		if (subject == NULL)
			subject = "skeleton output";
	} else if (subject != NULL) {
		errx(1, "The -s option may only be used along with -u");
	}
	if (uname != NULL || subject != NULL)
		errx(1, "TODO: mailing out the output to '%s' not supported yet", uname);

	/* OK, really process the command-line arguments */
	for (int i = 0; i < argc; i++) {
		debug("Processing %s\n", argv[i]);

		/* Yeah, the exec*() function prototypes are broken as to constness. */
		char * const headcmd[] = { strdup("head"), strdup("-n"), strdup(lines), argv[i], NULL };
		char * const cksumcmd[] = { strdup("cksum"), NULL };

		printf("%s\t", argv[i]);
		fflush(stdout);
		if (noop) {
			size_t j;

			for (j = 0; headcmd[j] != NULL; j++)
				printf("%s ", headcmd[j]);
			putchar('|');
			for (j = 0; cksumcmd[j] != NULL; j++)
				printf(" %s", cksumcmd[j]);
			putchar('\n');
			continue;
		}

		int headpipe[2], cksumpipe[2];
		pid_t headpid, cksumpid;
		pipe_prog(PIPE_DIR_READ, headpipe, &headpid, headcmd);
		pipe_prog(PIPE_DIR_WRITE, cksumpipe, &cksumpid, cksumcmd);

		ssize_t hn;
		while (hn = read(headpipe[0], buf, sizeof(buf)), hn != 0) {
			const size_t len = (size_t)hn;
			size_t pos = 0;
			do {
				const ssize_t n = write(cksumpipe[1],
				    buf + pos, len - pos);
				if (n == -1)
					err(1, "Writing to the pipe to cksum");
				pos += n;
			} while (pos < len);
		}
		if (hn == -1)
			err(1, "Reading from the pipe to head");
		if (close(headpipe[0]) == -1)
			err(1, "Closing the pipe to head");
		if (close(cksumpipe[1]) == -1)
			err(1, "Closing the pipe to cksum");

		int stat;
		pid_t res = waitpid(headpid, &stat, 0);
		check_wait_result(res, stat, headpid, "head");
		res = waitpid(cksumpid, &stat, 0);
		check_wait_result(res, stat, cksumpid, "cksum");
	}
	return (0);
}
