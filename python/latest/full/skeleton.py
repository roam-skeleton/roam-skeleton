#!/usr/bin/python3
#
# Copyright (c) 2017 - 2019  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
"""A skeleton template for writing command-line tools."""

import argparse
import os
import signal
import subprocess
import sys
import types

from typing import Dict


VERSION_STRING = "0.1.0.dev1"


def version() -> None:
    """Display program version information."""
    print("skeleton " + VERSION_STRING)


def features() -> None:
    """Display a list of the features supported by the program."""
    print("Features: skeleton=" + VERSION_STRING)


def sig_handler(_sig: signal.Signals, _frame: types.FrameType) -> None:
    """Signal handler: exit the program."""
    sys.exit(2)


def setup_signals() -> Dict[str, signal.Signals]:
    """Set up the signal handlers for the signals test."""
    sigs = {
        "SIGHUP": signal.SIGHUP,
        "SIGINT": signal.SIGINT,
        "SIGQUIT": signal.SIGQUIT,
        "SIGTERM": signal.SIGTERM,
    }
    for signum in sigs.values():
        signal.signal(signum, sig_handler)
    return sigs


def main() -> None:
    """Main program: parse arguments, read files, do stuff."""
    parser = argparse.ArgumentParser(
        prog="skeleton",
        usage="""
    skeleton filename...
    skeleton -V | -h | --version | --help
    skeleton --features""",
    )
    parser.add_argument(
        "-N", "--noop", action="store_true", help="no-operation mode"
    )
    parser.add_argument(
        "-S",
        "--signal",
        help="raise the specified signal (or NONE) before " "the actual work",
    )
    parser.add_argument(
        "-V",
        "--version",
        action="store_true",
        help="display program version information and exit",
    )
    parser.add_argument(
        "--features",
        action="store_true",
        help="display program feature information",
    )
    parser.add_argument(
        "-n",
        "--lines",
        type=int,
        default=10,
        help="specify the number of lines to read from each "
        "file (default: 10)",
    )
    parser.add_argument(
        "files", nargs="*", help="the names of the files to examine"
    )

    args = parser.parse_args()
    if args.version:
        version()
        sys.exit(0)

    if args.features:
        features()
        sys.exit(0)

    if not args.files or (args.lines is not None and args.lines < 1):
        parser.print_help(file=sys.stderr)
        sys.exit(1)

    sigs = setup_signals()
    if args.signal is not None and args.signal != "NONE":
        signame = args.signal
        signum = sigs.get(signame, sigs.get("SIG" + signame, None))
        if signum is None:
            sys.exit(
                'Invalid signal name "{sig}", should be one of {names}'.format(
                    sig=signame, names=", ".join(sorted(sigs.keys()))
                )
            )
        os.kill(os.getpid(), signum)

    for name in args.files:
        if args.noop:
            print("{name}\t0 0".format(name=name))
            continue

        subprocess.check_output(["wc", "--", name])
        print(name, end="\t")
        sys.stdout.flush()

        contents = ""
        with open(name, mode="r", encoding="Latin-1") as infile:
            for _ in range(args.lines):
                line = infile.readline()
                if line == "":
                    break
                contents += line

        cksum = subprocess.Popen(["cksum"], stdin=subprocess.PIPE)
        cksum.stdin.write(contents.encode("Latin-1"))
        cksum.stdin.close()
        cksum.wait()
        res = cksum.returncode
        if res != 0:
            sys.exit(
                "Could not calculate the checksum for {name}".format(name=name)
            )


main()
