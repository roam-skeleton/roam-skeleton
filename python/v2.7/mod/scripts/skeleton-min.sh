#!/bin/sh

basedir="$(dirname -- "$(dirname -- "$0")")"
ppath="$basedir${PYTHONPATH+:$PYTHONPATH}" \
smin="$basedir/scripts/skeleton_min.py"

exec env PYTHONPATH="$ppath" "$TOOL_NAME" -- "$smin" "$@"
