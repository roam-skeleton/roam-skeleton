#!/usr/bin/python
#
# Copyright (c) 2018, 2019  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
""" A command-line tool to demonstrate writing Python command-line tools. """

from __future__ import print_function

import argparse
import subprocess
import sys

import typing  # noqa pylint: disable=unused-import


VERSION_STRING = "0.1.0.dev1"


def version():
    # type: () -> None
    """Display program version information."""
    print("skeleton " + VERSION_STRING)


def features():
    # type: () -> None
    """Display a list of the features supported by the program."""
    print("Features: skeleton=" + VERSION_STRING)


def main():
    # type: () -> None
    """The main program: parse arguments, do things."""
    parser = argparse.ArgumentParser(
        prog="skeleton",
        usage="""
    skeleton filename...
    skeleton -V | -h | --help | --version
    skeleton --features""",
    )
    parser.add_argument(
        "-N", "--noop", action="store_true", help="no-operation mode"
    )
    parser.add_argument(
        "-V",
        "--version",
        action="store_true",
        help="display program version information and exit",
    )
    parser.add_argument(
        "--features",
        action="store_true",
        help="display program feature information",
    )
    parser.add_argument(
        "files", nargs="*", help="the names of the files to examine"
    )

    args = parser.parse_args()
    if args.version:
        version()
        sys.exit(0)

    if args.features:
        features()
        sys.exit(0)

    if not args.files:
        parser.print_help(file=sys.stderr)
        sys.exit(1)

    for name in args.files:
        if args.noop:
            print("{name}\t0 0".format(name=name))
            continue

        subprocess.check_call(["wc", "--", name])


main()
