#!/usr/bin/make -f
#
# Copyright (c) 2013, 2015 - 2018, 2020  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

C_FULL_BASE?=	c/full/
PROG=		${C_FULL_BASE}skeleton
SRCS=		${C_FULL_BASE}skeleton.c
OBJS=		${C_FULL_BASE}skeleton.o

C_MIN_BASE?=	c/min/
PROG_MIN=	${C_MIN_BASE}skeleton-min
SRCS_MIN=	${C_MIN_BASE}skeleton-min.c
OBJS_MIN=	${C_MIN_BASE}skeleton-min.o

RUST_FULL_BASE?=	rust/full/
RUST_TARGET?=	debug
PROG_RUST_FULL=	${RUST_FULL_BASE}target/${RUST_TARGET}/skeleton

RUST_FUNC_BASE?=	rust/func/
RUST_TARGET?=	debug
PROG_RUST_FUNC=	${RUST_FUNC_BASE}target/${RUST_TARGET}/skeleton

RM?=		rm -f
FIND?=		find
XARGS?=		xargs

CC?=		cc

CPPFLAGS_STD?=	-D_POSIX_C_SOURCE=200809L -D_XOPEN_SOURCE=700

CPPFLAGS+=	${CPPFLAGS_STD}

CFLAGS_OPT?=	-O2 -g -pipe
CFLAGS_STD?=	-std=c99
CFLAGS_WARN?=	-Wall -W -Wextra

CFLAGS?=	${CFLAGS_OPT}
CFLAGS+=	${CFLAGS_STD} ${CFLAGS_WARN}

#CFLAGS+=	-Werror
#CFLAGS+=	-pipe -Wall -W -std=c99 -pedantic -Wbad-function-cast \
#		-Wcast-align -Wcast-qual -Wchar-subscripts -Winline \
#		-Wmissing-prototypes -Wnested-externs -Wpointer-arith \
#		-Wredundant-decls -Wshadow -Wstrict-prototypes -Wwrite-strings

export CC PROG PROG_MIN PROG_RUST_FULL PROG_RUST_FUNC

all:
		./available-tools.sh build

list-available:
		@./available-tools.sh check

clean:
		${RM} ${PROG} ${PROG_MIN} ${OBJS} ${OBJS_MIN}
		${RM} ${RUST_FULL_BASE}Cargo.lock ${RUST_FUNC_BASE}Cargo.lock
		${RM} -r ${RUST_FULL_BASE}target/ ${RUST_FUNC_BASE}target/
		${RM} -r .mypy_cache/ .tox/
		${FIND} python/ -type d -name __pycache__ -print0 | \
		    ${XARGS} -0r ${RM} -r
		${FIND} python/ -type f -name '*.pyc' -delete

${PROG_RUST_FULL}:	${RUST_FULL_BASE}Cargo.toml ${RUST_FULL_BASE}src/config.rs ${RUST_FULL_BASE}src/main.rs
		if [ '${RUST_TARGET}' = 'release' ]; then \
			cd ${RUST_FULL_BASE} && cargo build --release; \
		else \
			cd ${RUST_FULL_BASE} && cargo build; \
		fi

${PROG_RUST_FUNC}:	${RUST_FUNC_BASE}Cargo.toml ${RUST_FUNC_BASE}src/config.rs ${RUST_FUNC_BASE}src/main.rs ${RUST_FUNC_BASE}src/output.rs
		if [ '${RUST_TARGET}' = 'release' ]; then \
			cd ${RUST_FUNC_BASE} && cargo build --release; \
		else \
			cd ${RUST_FUNC_BASE} && cargo build; \
		fi

test-single:	${TEST_PROG}
		echo "Testing ${TEST_PROG}"
		prove t
		echo "Testing ${TEST_PROG} complete"

test-real:	${PROG}
		${MAKE} test-single TEST_PROG="./${PROG}"

test-min:	${PROG_MIN}
		${MAKE} test-single TEST_PROG="./${PROG_MIN}"

test:		test-real test-min

test-all:
		./available-tools.sh test ${MAKE} test-single

${PROG}:	${OBJS}

${PROG_MIN}:	${OBJS_MIN}

.PHONY:		all clean test test-min test-real test-single test-all
