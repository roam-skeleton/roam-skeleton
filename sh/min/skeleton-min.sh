#!/bin/sh
#
# Copyright (c) 2013, 2015 - 2018  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

set -e

version_string='0.1.0.dev1'

version()
{
	echo "skeleton $version_string"
}

features()
{
	echo "Features: skeleton=$version_string"
}

usage()
{
	cat <<EOUSAGE
Usage:	skeleton [-Nv] ...
	skeleton -V | -h | --version | --help 
	skeleton --features

	-h	display program usage information and exit
	-N	no operation mode; display what would have been done
	-V	display program version information and exit
	-v	verbose operation; display diagnostic output
EOUSAGE
}

unset hflag noop Vflag v show_features

while getopts 'hNVv-:' o; do
	case "$o" in
		h)
			hflag=1
			;;

		N)
			noop='echo'
			;;

		V)
			Vflag=1
			;;

		v)
			v='-v'
			;;

		-)
			if [ "$OPTARG" = 'help' ]; then
				hflag=1
			elif [ "$OPTARG" = 'version' ]; then
				Vflag=1
			elif [ "$OPTARG" = 'features' ]; then
				show_features=1
			else
				echo "Invalid long option '$OPTARG' specified" 1>&2
				usage 1>&2
				exit 1
			fi
			;;

		*)
			usage 1>&2
			exit 1
			;;
	esac
done
[ -z "$Vflag" ] || version
[ -z "$hflag" ] || usage
[ -z "$show_features" ] || features
[ -z "$Vflag$hflag$show_features" ] || exit 0

shift `expr "$OPTIND" - 1`
if [ "$#" -lt 1 ]; then
	usage 1>&2
	exit 1
fi

tempf=`mktemp skeleton-output.txt.XXXXXX`
trap "rm -f '$tempf'" EXIT QUIT TERM INT HUP 

echo "skeleton operating on $# file arguments" > "$tempf"
while [ "$#" -gt 0 ]; do
	fname="$1"
	shift

	if [ -n "$noop" ] || [ -f "$fname" ]; then
		echo "- $fname" > "$tempf"
	else
		echo "'$fname' does not seem to be a file" 1>&2
		exit 1
	fi
done
echo 'Something something something tempfile...'
cat -- "$tempf"
