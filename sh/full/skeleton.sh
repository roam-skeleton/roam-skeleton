#!/bin/sh
#
# Copyright (c) 2013, 2015 - 2018  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

# Let the shell take care of most of the error handling in
# a really simple, straightforward way :P
#
# If we need some kind of cleaning up, well, there are trap handlers.

set -e

# Validation patterns
#
# I was hoping for a re_val_uint='\(0|[1-9][0-9]*\)$', but it turns out
# that expr(1) on at least Debian 7 and FreeBSD 10 does not support
# this kind of combination of grouping and alternatives at all!
#
# Note that expr(1) patterns are implicitly anchored at the start,
# so we don't want to start them off with a '^'; plus, it would
# kind of break with the expr "x$var" : "x$expr" way we do it :)
#
# However, they are *not* anchored at the end, so we need the '$'
# there to make sure that the pattern really validates all of the input.
#
re_val_pint='[1-9][0-9]*$'
re_val_username='[a-zA-Z0-9_.][a-zA-Z0-9_.]*$'

# Parameter defaults - specified here so that they may be used in
# the usage() function.
#
def_lines=10
def_subject='skeleton output'

version_string='0.1.0.dev1'

# Boilerplate :)

version()
{
	echo "skeleton $version_string"
}

features()
{
	echo "Features: skeleton=$version_string"
}

# I favor the BSD way of sorting command-line options in a usage message
# or a manual page synopsis section: first the ones that do not take
# arguments, then the ones that do, in either case sorted alphabetically
# and uppercase before lowercase.

usage()
{
	cat <<EOUSAGE
Usage:	skeleton [-NTv] [-n lines] [-S sig] [-s subject] [-u username|uid] filename...
	skeleton -V | -h | --version | --help 
	skeleton --features

	-h	display program usage information and exit
	-N	no operation mode; display what would have been done
	-n	number of lines to output (default: $def_lines)
	-S	raise the specified signal (or NONE) before any actual work
	-s	subject line for the e-mail message if -u is specified
		(default: '$def_subject')
	-T	run a trap handler query/reinstate/invoke test
	-u	username or ID to mail the output to
	-V	display program version information and exit
	-v	verbose operation; display diagnostic output
EOUSAGE
}

# Set the variables influenced by the command-line parameters to
# their default values.  Even for the ones that have no defaults,
# it is a very good idea to explicitly unset them here so that
# we don't end up inheriting something from the user's environment.
#
# However, do not set the subject line to the default, since we want
# to later make sure that it has *not* been specified without -u.
#
unset hflag noop signame subject uname uid Vflag v show_features
lines="$def_lines"

while getopts 'hNn:S:s:u:Vv-:' o; do
	case "$o" in
		h)
			hflag=1
			;;

		N)
			noop='echo'
			;;

		n)
			# OK, we need to validate a positive integer;
			# fortunately, we seem to have the correct regex
			# pattern on hand :)
			#
			# The reason for expr "x$var" : "x$expr" instead of just
			# expr "$var" : "$expr" is to make really, really sure
			# that broken expr(1) implementation will try to handle
			# e.g. var='-e' or var='--' in, uhm, special ways.
			# Unfortunately, we ourselves cannot use either -e or --
			# since it is, yeah, you guessed it, not portable.
			
			if ! expr "x$OPTARG" : "x$re_val_pint" > /dev/null; then
				echo 'The number of lines must be a positive integer' 1>&2
				exit 1
			fi
			lines="$OPTARG"
			;;

		S)
			if [ "$OPTARG" != 'NONE' ]; then
				signame="$OPTARG"
			fi
			;;

		s)
			if [ -z "$OPTARG" ]; then
				echo 'Empty subject line specified' 1>&2
				exit 1
			fi
			subject="$OPTARG"
			;;

		u)
			# Already set?
			if [ -n "$uname$uid" ]; then
				echo 'Duplicate username or ID specified' 1>&2
				exit 1
			fi

			# Check for a user ID first, because, well, a numeric
			# user ID is actually a valid username!
			#
			# See the comment about unsigned integers above; that's
			# why we have to resort to = 0 or is positive...

			if [ "$OPTARG" = '0' ] || expr "x$OPTARG" : "x$re_val_uint" > /dev/null; then
				uid="$OPTARG"
			else
				if expr "x$OPTARG" : "x$re_val_username" > /dev/null; then
					uname="$OPTARG"
				else
					echo 'Invalid username or ID specified' 1>&2
					exit 1
				fi
			fi
			;;

		V)
			Vflag=1
			;;

		v)
			# Not demonstrated in this script, but really handy
			# for stuff like e.g.
			#
			# chown $v "$uname" "$fname"
			#
			# ...without quoting the $v - works just fine if $v
			# is empty (by default), the shell will not pass it
			# to the command at all.

			v='-v'
			;;

		-)
			if [ "$OPTARG" = 'help' ]; then
				hflag=1
			elif [ "$OPTARG" = 'version' ]; then
				Vflag=1
			elif [ "$OPTARG" = 'features' ]; then
				show_features=1
			else
				echo "Invalid long option '$OPTARG' specified" 1>&2
				usage 1>&2
				exit 1
			fi
			;;

		*)
			usage 1>&2
			exit 1
			;;
	esac
done
# If -V and/or -h is specified, print the request info and exit.
[ -z "$Vflag" ] || version
[ -z "$hflag" ] || usage
[ -z "$show_features" ] || features
[ -z "$Vflag$hflag$show_features" ] || exit 0

# Skip to the positional arguments.
shift `expr "$OPTIND" - 1`
if [ "$#" -lt 1 ]; then
	usage 1>&2
	exit 1
fi

if [ -n "$uid" ]; then
	[ -z "$v" ] || echo "Getting the login name for user ID $uid" 1>&2

	# We really hope that getent(1) will only return a single match.
	# Tested at least on Debian 7 and FreeBSD 10.
	#
	uname=`getent passwd "$uid" | cut -d: -f1`
	if [ -z "$uname" ]; then
		echo "Nonexistent user ID $uid" 1>&2
		exit 1
	fi
	[ -z "$v" ] || echo "- got $uname" 1>&2
fi

# Interplay between the command-line arguments.  This cannot really
# be handled above since we don't know what order the options will
# be specified in, and we don't want to repeat the checks.

if [ -n "$uname" ]; then
	if [ -z "$subject" ]; then
		subject='skeleton output'
	fi
else
	if [ -n "$subject" ]; then
		echo 'The -s option makes no sense without the -u one' 1>&2
		exit 1
	fi
fi

# Create temporary files, remove them on exit or if something bad happens.
#
# Unfortunately, mktemp(1)'s syntax for using a temporary directory is
# not standardized at all and actually varies - the -t option may or may
# not take an argument, the directory may or may not need to be specified
# explicitly, etc...  So just go with the current directory and hope that
# nothing kills the shell itself before it cleans up in the trap handler.
#
# And yes, there are ways to use 'trap' to get the current trap handler
# command and then prepend ours, but I think this is just that little bit of
# an overkill for *this* trivial case.

unset tempf tempsum cleanup_err

cleanup() {
	local res="$?"
	local arg="$1"

	if [ -n "$tempf" ]; then
		rm -f -- "$tempf"
		unset tempf
	fi
	if [ -n "$tempsum" ]; then
		rm -f -- "$tempsum"
		unset tempsum
	fi

	if [ "$arg" -eq 0 ] && [ -n "$res" ]; then
		arg="$res"
	fi
	: "${cleanup_err:=$arg}"
	exit "$cleanup_err"
}

trap 'cleanup 0' EXIT
trap 'cleanup 2' QUIT TERM INT HUP

tempf=`mktemp skeleton-output.txt.XXXXXX`
tempsum=`mktemp skeleton-tempsum.txt.XXXXXX`

# And now for the real work - process the positional arguments.
#
# Store the output into the tempfile and later cat or mail it.
# We *might* have done a while ... | if ... then cat; else mail; fi
# but then we might lose the error status if anything bad happens in
# the while loop itself.  Right, 'pipefail' is not portable.

if [ -n "$signame" ]; then
	kill "-$signame" "$$"
fi

while [ "$#" -gt 0 ]; do
	fname="$1"
	shift
	[ -z "$v" ] || echo "Processing $fname" 1>&2

	# OK, this really ought to be handled by a pipe, but here's
	# a little demonstration of how $noop is used :)

	$noop printf '%s\t' "$fname"
	$noop head -n "$lines" "$fname" > "$tempsum"
	$noop cksum < "$tempsum"
done > "$tempf"

if [ -z "$uname" ]; then
	cat "$tempf"
else
	[ -z "$v" ] || echo "Mailing the output to $uname" 1>&2
	$noop mail -s "$subject" "$uname" < "$tempf"
	[ -z "$noop" ] || cat "$tempf"
fi
