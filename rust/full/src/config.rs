/**
 * Copyright (c) 2017, 2018  Peter Pentchev
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

use std::env;
use std::error::Error;
use std::io::{Write, stderr};
use std::process::exit;

use getopts;

pub struct Config {
	pub args: Vec<String>,
	pub noop: bool,
	pub verbose: bool,
}

impl Config {
	pub fn debug(&self, s: &str) {
		if self.verbose {
			match writeln!(stderr(), "{}", s) {
				Ok(_) => (),
				Err(_) => (),
			}
		}
	}
}

pub const MSG_VERSION: &'static str = "skeleton 0.1.0.dev1\n";
pub const MSG_FEATURES: &'static str = "Features: skeleton=0.1.0.dev1\n";
pub const MSG_USAGE: &'static str = "Usage:	skeleton [-Nv] filename...
	skeleton -V | -h | --version | --help 
	skeleton --features

	-h	display program usage information and exit
	-N	no-operation mode; display what would have been done
	-V	display program version information and exit
	-v	verbose mode; display diagnostic information
";

fn build_options_parser() -> getopts::Options {
	let mut opts = getopts::Options::new();
	opts.parsing_style(getopts::ParsingStyle::StopAtFirstFree);
	opts.optflag("h", "help", "");
	opts.optflag("N", "", "");
	opts.optflag("V", "version", "");
	opts.optflag("v", "", "");
	opts.optflag("", "features", "");
	opts
}

pub fn die(s: &str) -> ! {
	match writeln!(stderr(), "{}", s) {
		Ok(_) => (),
		Err(_) => (),
	};
	exit(1);
}

fn parse_options(m: getopts::Matches) -> Config {
	match m.free.len() {
		0 => die(MSG_USAGE),

		_ => Config {
			args: m.free.iter().cloned().collect(),
			noop: m.opt_present("N"),
			verbose: m.opt_present("v"),
		},
	}
}

fn parse_help_version(m: getopts::Matches) -> Config {
	let has_help = m.opt_present("h");
	let help = if has_help { MSG_USAGE } else { "" };
	let has_version = m.opt_present("V");
	let version = if has_version { MSG_VERSION } else { "" };
	let has_features = m.opt_present("features");
	let features = if has_features { MSG_FEATURES } else { "" };

	match has_help || has_version || has_features {
		true => {
			print!("{}{}{}", version, help, features);
			exit(0);
		},

		false => parse_options(m),
	}
}

pub fn get_options() -> Config {
	let args: Vec<String> = env::args().collect();
	let opts = build_options_parser();
	match opts.parse(&args[1..]) {
		Ok(m) => parse_help_version(m),

		Err(e) => die(&format!(
			"Could not parse the command-line options: {}\n{}",
			e.description(), MSG_USAGE)),
	}
}
