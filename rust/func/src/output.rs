/**
 * Copyright (c) 2017, 2018  Peter Pentchev
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

use std::io::{Write, stderr, stdout};

use config;

#[derive(Clone)]
pub struct Message {
	pub err: bool,
	pub msg: String,
}

pub fn m_ok(s: &str) -> Message {
	Message { err: false, msg: String::from(s), }
}

pub fn m_err(s: &str) -> Message {
	Message { err: true, msg: String::from(s), }
}

pub fn m_version() -> Message {
	m_ok(config::MSG_VERSION)
}

pub fn m_features() -> Message {
	m_ok(config::MSG_FEATURES)
}

pub fn m_usage(err: bool) -> Message {
	Message { err: err, msg: String::from(config::MSG_USAGE), }
}

pub fn display_and_exit(data: &[Message]) -> i32 {
	if data.iter().filter(|m| m.err).any(|m|
		writeln!(stderr(), "{}", m.msg).is_err()
	) {
		return 255;
	}
	if data.iter().filter(|m| !m.err).any(|m|
		writeln!(stdout(), "{}", m.msg).is_err()
	) {
		return 255;
	}
	if data.iter().any(|m| m.err) { 1 } else { 0 }
}
