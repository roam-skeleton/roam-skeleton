The skeleton utility is a sample shell script to demonstrate the author's
habits of, well, writing shell scripts.  It is meant to be a library of
miniature snippets (e.g. parsing options, creating temporary files, validating
user input, etc).  It is by pure coincidence that it actually does something,
although it is a bit doubtful if said something is indeed anything useful.

Comments: Peter Pentchev <roam@ringlet.net>
