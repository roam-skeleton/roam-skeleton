#!/usr/bin/perl
#
# Copyright (c) 2017  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

use v5.010;
use strict;
use warnings;

use File::Basename qw(basename);
use Test::More;
use Test::Command;

my $prog = $ENV{TEST_PROG} // './skeleton';

my $basename = (split /\./, basename($prog), 2)[0];
if ($basename ne 'skeleton') {
	plan skip_all => 'Only testing the signal handlers with full-fledged implementations';
} elsif ($prog =~ m{target/.*/skeleton}) {
	plan skip_all => 'No real signal handling in Rust yet';
} else {
	plan tests => 3;
}

sub get_ok_output($ $) {
	my ($cmd, $desc) = @_;

	my $c = Test::Command->new(cmd => $cmd);
	$c->exit_is_num(0, "$desc succeeded");
	$c->stderr_is_eq('', "$desc did not output any errors");
	split /\n/, $c->stdout_value
}

sub get_error_output($ $; $) {
	my ($cmd, $desc, $code) = @_;

	my $c = Test::Command->new(cmd => $cmd);
	if (defined $code) {
		$c->exit_is_num($code, "$desc failed with exit code $code");
	} else {
		$c->exit_isnt_num(0, "$desc failed");
	}
	$c->stdout_is_eq('', "$desc did not output anything");
	split /\n/, $c->stderr_value
}

# More than one usage line with -h
subtest 'Fail with -S with no filename' => sub {
	my @lines = get_error_output([$prog, '-S', 'NONE'], '-S with no filename');
	ok @lines, '-S with no filename reported errors';
};

subtest 'Succeed with -S NONE' => sub {
	my @lines = get_ok_output([$prog, '-S', 'NONE', 'Makefile', 'README.txt'], '-S NONE');
	is @lines, 2, '-S NONE output as many lines as there were filename arguments'
};

subtest 'Fail with -S QUIT' => sub {
	get_error_output([$prog, '-S', 'QUIT', 'Makefile', 'README.txt'], '-S QUIT', 2);
};
