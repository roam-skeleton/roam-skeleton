#!/usr/bin/perl
#
# Copyright (c) 2017  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

use v5.010;
use strict;
use warnings;

use Test::More;
use Test::Command;

my $prog = $ENV{TEST_PROG} // './skeleton';

my @usage_lines;

sub get_ok_output($ $) {
	my ($cmd, $desc) = @_;

	my $c = Test::Command->new(cmd => $cmd);
	$c->exit_is_num(0, "$desc succeeded");
	$c->stderr_is_eq('', "$desc did not output any errors");
	split /\n/, $c->stdout_value
}

sub get_error_output($ $) {
	my ($cmd, $desc) = @_;

	my $c = Test::Command->new(cmd => $cmd);
	$c->exit_isnt_num(0, "$desc failed");
	$c->stdout_is_eq('', "$desc did not output anything");
	split /\n/, $c->stderr_value
}

plan tests => 6;

my $is_python = $prog =~ m{ /python/ }x;

# More than one usage line with -h
subtest 'Usage output with -h' => sub {
	my $c = Test::Command->new(cmd => [$prog, '-h']);
	$c->exit_is_num(0, '-h succeeded');
	$c->stderr_is_eq('', '-h did not output any errors');
	my @lines = split /\n/, $c->stdout_value;
	BAIL_OUT('Too few lines in the -h output') unless @lines > 1;
	if (!$is_python) {
		BAIL_OUT('Unexpected -h first line') unless $lines[0] =~ /^ Usage: \s+ skeleton /x;
	} else {
		BAIL_OUT('Unexpected -h first line') unless $lines[0] =~ /^ usage: \s+ $/x;
	}
	@usage_lines = @lines;
};

subtest 'Fail with no filename specified' => sub {
	my @lines = get_error_output([$prog], 'no filename specified');
	is_deeply \@lines, \@usage_lines, 'no filename output the usage message';
};

subtest 'No-operation mode' => sub {
	my @lines = get_ok_output([$prog, '-N', 'Makefile', 'README.txt'], 'no-operation mode');
	ok scalar @lines >= 2, '-N output at least as many lines as filenames provided';
};

subtest 'Real work' => sub {
	my @lines = get_ok_output([$prog, 'Makefile', 'README.txt'], 'real work');
	is scalar @lines, 2, 'a real invocation output as many lines as filenames provided';
};

subtest 'False success on a nonexistent file in no-op mode' => sub {
	my @lines = get_ok_output([$prog, '-N', 'nonexistent'], 'no-operation mode, nonexistent file');
	ok @lines, '-N output at least one line with a nonexistent file';
};

subtest 'Fail on a nonexistent file' => sub {
	my $c = Test::Command->new(cmd => [$prog, 'nonexistent']);
	$c->exit_isnt_num(0, 'failed on a nonexistent file');
	$c->stderr_isnt_eq('', 'output error messages on a nonexistent file');
};
