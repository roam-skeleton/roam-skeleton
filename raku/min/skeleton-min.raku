#!/usr/bin/env raku
#
# Copyright (c) 2016 - 2018  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

use v6;

use Getopt::Std;
use Shell::Capture;

constant VERSION-STRING = '0.1.0.dev1';

my Bool ($debug, $noop);

sub version()
{
	say 'skeleton ' ~ VERSION-STRING;
}

sub usage(Bool:D $err = True)
{
	my $s = q:to/EOUSAGE/;
Usage:	skeleton [-Nv] file...
	skeleton -V | -h

	-h	display program usage information and exit
	-N	no operation mode; display what would have been done
	-V	display program version information and exit
	-v	verbose mode; display diagnostic output
EOUSAGE

	if $err {
		$*ERR.print($s);
		exit 1;
	} else {
		print $s;
	}
}

sub show-features()
{
	say 'Features: skeleton=' ~ VERSION-STRING;
}

sub note-fatal(Str:D $s)
{
	note $s;
	exit 1;
}

sub debug(Str:D $s)
{
	note $s if $debug;
}

sub help_or_version(Hash $opts)
{
	my Bool:D $has_dash = defined $opts<->;
	my Bool:D $dash_help = $has_dash && $opts<-> eq 'help';
	my Bool:D $dash_version = $has_dash && $opts<-> eq 'version';
	my Bool:D $dash_features = $has_dash && $opts<-> eq 'features';

	if $has_dash && !$dash_help && !$dash_version && !$dash_features {
		note "Invalid long option '$opts<->' specified";
		usage True;
	}
	version if $opts<V> || $dash_version;
	usage False if $opts<h> || $dash_help;
	show-features if $dash_features;
	exit(0) if $opts{<V h ->}:k;
}

{
	my %opts = do {
		CATCH { when X::Getopt::Std { .message.note; usage; } };
		getopts('hNVv-:', @*ARGS)
	};
	help_or_version %opts;

	$noop = ?%opts<N>;
	$debug = ?%opts<v>;

	usage unless @*ARGS;

	my UInt:D $count-okay = 0;
	for @*ARGS -> $fname {
		debug "Processing $fname";
		if $noop {
			say "- would run wc -l -- $fname";
			$count-okay++;
			next;
		}
		my Shell::Capture $c .= capture('wc', '-l', '--', $fname);
		given $c {
			when .exitcode != 0 {
				note "Could not count the lines in $fname";
			}

			when .lines.elems != 1 {
				note "Unexpected number of lines from wc -l $fname: expected 1, got " ~ .lines.elems;
			}

			when .lines[0] ~~ /^ $<num> = [ \d+ ] \s / {
				say "Got $/<num> lines for $fname";
				$count-okay++;
			}

			default {
				note "Unexpected output from wc -l $fname: " ~ .lines[0];
			}
		}
	}
	if $count-okay != @*ARGS.elems {
		note 'Errors found';
		exit 1;
	}
}
