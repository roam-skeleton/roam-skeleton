#!/usr/bin/perl
#
# Copyright (c) 2016 - 2018  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

use v5.10;
use strict;
use warnings;

use Getopt::Std;

use constant VERSION_STRING => '0.1.0.dev1';

my $debug = 0;

sub usage($)
{
	my ($err) = @_;
	my $s = <<EOUSAGE
Usage:	skeleton [-Nv] filename...
	skeleton -V | -h | --version | --help
	skeleton --features

	-h	display program usage information and exit
	-N	no-operation mode
	-V	display program version information and exit
	-v	verbose operation; display diagnostic output
EOUSAGE
	;

	if ($err) {
		die $s;
	} else {
		print "$s";
	}
}

sub version()
{
	say 'skeleton '.VERSION_STRING;
}

sub features()
{
	say 'Features: skeleton='.VERSION_STRING;
}

sub debug($)
{
	say STDERR "RDBG $_[0]" if $debug;
}

sub check_wait_result($ $ $)
{
	my ($stat, $pid, $name) = @_;
	my $sig = $stat & 127;
	if ($sig != 0) {
		die "Program '$name' (pid $pid) was killed by signal $sig\n";
	} else {
		my $code = $stat >> 8;
		if ($code != 0) {
			die "Program '$name' (pid $pid) exited with non-zero status $code\n";
		}
	}
}

sub run_command(@)
{
	my (@cmd) = @_;
	my $name = $cmd[0];

	my $pid = open my $f, '-|';
	if (!defined $pid) {
		die "Could not fork for $name: $!\n";
	} elsif ($pid == 0) {
		debug "About to run '@cmd'";
		exec { $name } @cmd;
		die "Could not execute '$name': $!\n";
	}
	my @res = <$f>;
	close $f;
	check_wait_result $?, $pid, $name;
	return @res;
}

sub help_or_version($)
{
	my ($opts) = @_;
	my $has_dash = defined $opts->{'-'};
	my $dash_help = $has_dash && $opts->{'-'} eq 'help';
	my $dash_version = $has_dash && $opts->{'-'} eq 'version';
	my $dash_features = $has_dash && $opts->{'-'} eq 'features';
	
	if ($has_dash && !$dash_help && !$dash_version && !$dash_features) {
		warn "Invalid long option '".$opts->{'-'}."' specified\n";
		usage 1;
	}
	version if $opts->{V} || $dash_version;
	usage 0 if $opts->{h} || $dash_help;
	features if $dash_features;
	exit 0 if $opts->{V} || $opts->{h} || $has_dash;
}

MAIN:
{
	my %opts;

	getopts('hNVv-:', \%opts) or usage 1;
	help_or_version \%opts;
	$debug = $opts{v};

	usage 1 unless @ARGV;

	for my $fname (@ARGV) {
		if ($opts{N}) {
			say "$fname ok";
			next;
		}
		die "'$fname' is not a file" unless -f $fname;
		say "$fname ok";
	}
}
