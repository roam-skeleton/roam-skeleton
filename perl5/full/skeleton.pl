#!/usr/bin/perl
#
# Copyright (c) 2013, 2015 - 2018  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

use v5.10;
use strict;
use warnings;

use Getopt::Std;

use constant VERSION_STRING => '0.1.0.dev1';

my $debug = 0;

sub usage($)
{
	my ($err) = @_;
	my $s = <<EOUSAGE
Usage:	skeleton [-Nv] [-n lines] [-S signame] [-s subject] [-u username|id] filename...
	skeleton -V | -h | --version | --help 
	skeleton --features

	-h	display program usage information and exit
	-N	no-operation mode
	-n	specify the number of lines to read from each file (default: 10)
	-S	raise the specified signal (or NONE) before the actual work
	-s	specify the e-mail subject (default: "skeleton output")
	-u	mail the output to the specified user
	-V	display program version information and exit
	-v	verbose operation; display diagnostic output
EOUSAGE
	;

	if ($err) {
		die $s;
	} else {
		print "$s";
	}
}

sub version()
{
	say 'skeleton '.VERSION_STRING;
}

sub features()
{
	say 'Features: skeleton='.VERSION_STRING;
}

sub debug($)
{
	say STDERR "RDBG $_[0]" if $debug;
}

sub check_wait_result($ $ $)
{
	my ($stat, $pid, $name) = @_;
	my $sig = $stat & 127;
	if ($sig != 0) {
		die "Program '$name' (pid $pid) was killed by signal $sig\n";
	} else {
		my $code = $stat >> 8;
		if ($code != 0) {
			die "Program '$name' (pid $pid) exited with non-zero status $code\n";
		}
	}
}

sub run_command(@)
{
	my (@cmd) = @_;
	my $name = $cmd[0];

	my $pid = open my $f, '-|';
	if (!defined $pid) {
		die "Could not fork for $name: $!\n";
	} elsif ($pid == 0) {
		debug "About to run '@cmd'";
		exec { $name } @cmd;
		die "Could not execute '$name': $!\n";
	}
	my @res = <$f>;
	close $f;
	check_wait_result $?, $pid, $name;
	return @res;
}

sub help_or_version($)
{
	my ($opts) = @_;
	my $has_dash = defined $opts->{'-'};
	my $dash_help = $has_dash && $opts->{'-'} eq 'help';
	my $dash_version = $has_dash && $opts->{'-'} eq 'version';
	my $dash_features = $has_dash && $opts->{'-'} eq 'features';
	
	if ($has_dash && !$dash_help && !$dash_version && !$dash_features) {
		warn "Invalid long option '".$opts->{'-'}."' specified\n";
		usage 1;
	}
	version if $opts->{V} || $dash_version;
	usage 0 if $opts->{h} || $dash_help;
	features if $dash_features;
	exit 0 if $opts->{V} || $opts->{h} || $has_dash;
}

sub setup_signals()
{
	for my $sig (qw(HUP INT QUIT TERM)) {
		$SIG{$sig} = sub { exit(2); };
	}
}

MAIN:
{
	my ($nlines, $fname, $subj, $mailuser, $mailpid, $outf, $res);
	my %opts;

	getopts('hNn:S:s:u:Vv-:', \%opts) or usage 1;
	help_or_version \%opts;
	$debug = $opts{v};

	if (defined $opts{n}) {
		if ($opts{n} !~ /^[1-9][0-9]*$/) {
			die "Invalid number of lines, should be a positive integer\n";
		}
		$nlines = $opts{n};
	} else {
		$nlines = 10;
	}

	if (defined $opts{u}) {
		debug "Checking user name or ID $opts{u}";
		if ($opts{u} =~ /^(0|[1-9][0-9]*$)$/) {
			debug "- user ID?";
			$mailuser = getpwuid $opts{u};
			if (!defined $mailuser) {
				die "Nonexistent user ID $opts{u}\n";
			}
		} elsif ($opts{u} !~ /^[A-Za-z_.][A-Za-z0-9_.]+$/) {
			die "Invalid user name or ID $opts{u}\n";
		} else {
			$mailuser = $opts{u};
		}
		debug "- got username $mailuser";

		if (defined $opts{s}) {
			$subj = $opts{s};
		} else {
			$subj = 'skeleton output';
		}
	} elsif (defined $opts{s}) {
		die "The -s option may only be used along with -u\n";
	}

	usage 1 unless @ARGV;

	setup_signals();
	if (defined $opts{S} && $opts{S} ne 'NONE') {
		kill $opts{S}, $$;
	}

	if (defined $mailuser) {
		$mailpid = open \*STDOUT, '|-';
		if (!defined $mailpid) {
			die "Could not fork for mail: $!\n";
		} elsif ($mailpid == 0) {
			exec { 'mail' } ('mail', '-s', $subj, $mailuser) or
			    die "Executing mail -s '$subj' $mailuser: $!\n";
		}
	}

	foreach $fname (@ARGV) {
		my ($f, $s, $pid);

		debug "Processing $fname";
		if ($opts{N}) {
			say "$fname   0 0";
			next;
		}
		run_command 'wc', '--', $fname;
		if ($fname ne '-') {
			open $f, '<', $fname or
			    die "Opening $fname: $!\n";
		} else {
			$f = \*STDIN;
		}
		$s = '';
		for (1..$nlines) {
			my $line = <$f>;

			last unless defined $line;
			$s .= $line;
		}
		if ($fname ne '-') {
			close $f or
			    die "Closing $fname: $!\n";
		}

		print "$fname\t";
		$pid = open $f, '|-';
		if (!defined $pid) {
			die "Forking for cksum: $!\n";
		} elsif ($pid == 0) {
			exec { 'cksum' } ('cksum') or
			    die "Executing cksum: $!\n";
		}
		print $f $s or
		    die "Piping $fname to cksum: $!\n";
		close $f or
		    die "Closing the cksum pipe for $fname: $!\n";
		$res = $?;
		check_wait_result $res, $pid, 'cksum';
	}

	if (defined $mailuser) {
		close STDOUT or
		    die "Closing the mail pipe: $!\n";
		$res = $?;
		check_wait_result $res, $mailpid, 'mail';
	}
}
