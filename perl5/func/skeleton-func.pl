#!/usr/bin/perl
#
# Copyright (c) 2016 - 2018  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

use v5.10;
use strict;
use warnings;

use Getopt::Std;
use List::MoreUtils qw(part);
use List::Util qw(max);

use constant usage_message => <<EOUSAGE ;
Usage:	skeleton [-Nv] filename...
	skeleton -V | -h | --version | --help 
	skeleton --features

	-h	display program usage information and exit
	-N	no-operation mode; do not actually collect file info
	-V	display program version information and exit
	-v	verbose operation; display diagnostic output
EOUSAGE

use constant version_string => '0.1.0.dev1';

use constant version_message => 'skeleton '.version_string;

use constant features_message => 'Features: skeleton='.version_string;

sub check_wait_result($ $ $)
{
	my ($stat, $pid, $name) = @_;
	my $sig = $stat & 127;
	$sig != 0
		? die "Program '$name' (pid $pid) was killed by signal $sig\n"
		: do {
			my $code = $stat >> 8;
			$code != 0
				? die "Program '$name' (pid $pid) exited with non-zero status $code\n"
				: 1
		}
}

sub run_command($ @)
{
	my ($cfg, @cmd) = @_;
	my $name = $cmd[0];

	my $pid = open my $f, '-|';
	defined($pid)
		? $pid == 0
			? do {
				$cfg->{debug}->("About to run '@cmd'");
				exec { $name } @cmd;
				die "Could not execute '$name': $!\n";
			}
			: do {
				my @res = <$f>;
				close $f;
				check_wait_result $?, $pid, $name;
				@res
			}
		: die "Could not fork for $name: $!\n"
}

sub parse_options($ $ $)
{
	my ($opts, $args, $callback) = @_;

	@{$args}
		? $callback->({
			debug => $opts->{v}? sub { say STDERR "RDBG $_[0]" }: sub {},
			files => [ @{$args} ],
			noop => $opts->{N}? sub { $_[0]->() }: sub { $_[1]->() },
			display => {
				# filestruct -- exitcode
				ok => sub { [0, "File $_[0]->{name}: size $_[0]->{size}"] },
				error => sub { [1, "File $_[0]->{name}: $_[0]->{message}"] },
			},
		})
		: ([1, usage_message])
}

sub process_files($ $)
{
	my ($cfg, $callback) = @_;

	$callback->($cfg, [ map {
		my $fname = $_;
		$cfg->{debug}("Processing $fname");

		$cfg->{noop}(
			sub {
				+{
					name => $fname,
					status => 'ok',
					size => 0,
				},
			},
			sub {
				my @stat = stat $fname;
				$cfg->{debug}('Got ' . ($stat[7] // 'nothing'));
				+{
					name => $fname,
					@stat
						? (
							status => 'ok',
							size => $stat[7],
						)
						: (
							status => 'error',
							message => "Could not examine $fname: $!",
						),
				}
			})
	} @{$cfg->{files}} ]);
}

sub display_results($ $)
{
	my ($cfg, $data) = @_;

	map { $cfg->{display}->{$_->{status}}->($_) } @{$data};
}

sub get_options($)
{
	my ($callback) = @_;

	my %opts;
	getopts('hNVv-:', \%opts)
		? $callback->(\%opts, \@ARGV)
		: ([1, usage_message])
}

sub parse_opts_ver_help($ $ $)
{
	my ($opts, $args, $callback) = @_;
	my $has_dash = defined $opts->{'-'};
	my $dash_help = $has_dash && $opts->{'-'} eq 'help';
	my $dash_version = $has_dash && $opts->{'-'} eq 'version';
	my $dash_features = $has_dash && $opts->{'-'} eq 'features';
	my @msg = (
		($opts->{V} || $dash_version ? ([0, version_message]) : ()),
		($opts->{h} || $dash_help ? ([0, usage_message]) : ()),
		($dash_features ? ([0, features_message]) : ()),
	);

	$has_dash && !$dash_help && !$dash_version && !$dash_features
		? (
			[1, "Invalid long option '".$opts->{'-'}."' specified"],
			[1, usage_message],
		)
		: @msg
			? @msg
			: $callback->($opts, $args)
}

sub display_and_exit(@)
{
	my @res = @_;
	exit 0 unless @res;

	my @by_res = part { $_->[0] ? 1 : 0 } @res;

	say STDERR $_->[1] for @{$by_res[1] || []};
	say $_->[1] for @{$by_res[0] || []};
	exit max map $_->[0], @res;
}

MAIN:
{
	display_and_exit get_options sub {
		# opts, args -- output
		parse_opts_ver_help $_[0], $_[1], sub {
			# opts, args -- output
			parse_options $_[0], $_[1], sub {
				# cfg -- output
				process_files $_[0], sub {
					# cfg, data -- output
					display_results $_[0], $_[1];
				}
			}
		}
	};
}
